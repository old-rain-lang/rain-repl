# rain-repl

A repl for the [`rain` intermediate representation](https://gitlab.com/rain-lang/rain-ir), designed as an example and integration test.

## Planned features
- Optional LLVM/WASM/Cranelift-backend code-generation with Julia-style dynamic function compilation
- Usability as both a binary and a library for building applications with a builtin `rain` REPL
- TUI mode, simple CLI mode and pipe/script mode
- IDE/Language Server integration