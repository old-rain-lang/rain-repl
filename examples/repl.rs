/*!
A REPL for the `rain` programming language
*/

#![deny(missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]

use clap::{App, Arg};
use rain_builder::Builder;
use rain_ir::value::ValueEnum;
use rain_repl::Repl;
use rustyline::error::ReadlineError;
use rustyline::Editor;
use std::error::Error;
use std::mem::size_of;

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("rain repl")
        .version("0.0.0")
        .author("Jad Ghalayini <jad.ghalayini@hotmail.com>")
        .about("repl for rain programs")
        .arg(
            Arg::with_name("history")
                .short("h")
                .long("history")
                .value_name("FILE")
                .help("Sets a file to save/load history")
                .takes_value(true),
        )
        .get_matches();
    let history = matches.value_of("history");
    let mut rl = Editor::<()>::new();
    let mut repl = Repl::new(Builder::new());

    println!("rain-repl version 0.0.0");
    println!(
        "pointer size: {}, ValueEnum size: {}",
        size_of::<*const u8>(),
        size_of::<ValueEnum>()
    );

    if let Some(history) = history {
        if rl.load_history(history).is_err() {
            println!("No previous history loaded from {:?}.", history);
        } else {
            println!("Loaded history from {:?}.", history)
        }
    } else {
        println!("No previous history loaded.")
    }
    loop {
        let line = rl.readline(repl.prompt());
        match line {
            Ok(line) => {
                if repl.handle_line(line.as_str(), &mut std::io::stdout())? {
                    rl.add_history_entry(repl.buffer());
                    repl.clear_buffer();
                }
            }
            Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
            Err(err) => {
                eprintln!("Error: {:?}", err);
                break;
            }
        }
    }
    if let Some(history) = history {
        rl.save_history(history).unwrap();
    }
    Ok(())
}
