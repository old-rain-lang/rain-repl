/*!
A REPL for the `rain` programming language
*/

#![deny(missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]

use std::io::Read;

use nom::branch::alt;
use nom::bytes::complete::{tag, take_until};
use nom::character::complete::{multispace0, multispace1};
use nom::combinator::{map, opt};
use nom::sequence::{delimited, preceded, separated_pair, terminated};
use nom::{Err, IResult};
use rain_ast::ast::{Expr, Statement};
use rain_ast::parser::{parse_bool, parse_expr, parse_statement};
use rain_builder::Builder;

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::io::Write;

/// A repl statement
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ReplStatement<'a> {
    /// A `rain` statement
    Statement(Statement<'a>),
    /// A `rain` expression
    Expr(Expr<'a>),
    /// A meta-command to the REPL
    Command(Command),
}

/// A repl command
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// Include the contents of a file
    Include(String),
    /// Print the current state of the builder
    PrintBuilderState(bool),
    /// Show functions defined
    ShowDefinitions(bool),
    /// Show parse results
    ShowParse(bool),
}

/// Parse a repl statement
pub fn parse_repl_statement(input: &str) -> IResult<&str, ReplStatement> {
    terminated(
        alt((
            map(parse_expr, ReplStatement::Expr),
            map(parse_statement, ReplStatement::Statement),
            map(parse_command, ReplStatement::Command),
        )),
        multispace0,
    )(input)
}

/// Parse a repl command
pub fn parse_command(input: &str) -> IResult<&str, Command> {
    delimited(
        delimited(multispace0, tag("#"), multispace0),
        alt((
            map(
                separated_pair(tag("show_parse"), multispace1, opt(parse_bool)),
                |(_, b)| Command::ShowParse(b.unwrap_or(true)),
            ),
            map(
                separated_pair(tag("builder_state"), multispace1, opt(parse_bool)),
                |(_, b)| Command::PrintBuilderState(b.unwrap_or(true)),
            ),
            map(
                separated_pair(tag("show_definitions"), multispace1, opt(parse_bool)),
                |(_, b)| Command::ShowDefinitions(b.unwrap_or(true)),
            ),
            map(
                separated_pair(
                    tag("include"),
                    multispace0,
                    delimited(tag("<"), take_until(">"), tag(">")),
                ),
                |(_, f)| Command::Include(String::from(f)),
            ),
        )),
        preceded(multispace0, tag(";")),
    )(input)
}

/// A very simple repl for the `rain` IR
#[derive(Debug)]
pub struct Repl<B> {
    buffer: String,
    builder: B,
    cursor: usize,
    config: ReplConfig,
    prompt: &'static str,
}

const DEFAULT_PROMPT: &str = ">>> ";

/// Struct that contains the configuration for repl
#[derive(Debug)]
pub struct ReplConfig {
    show_parse: bool,
    show_definitions: bool,
}

impl ReplConfig {
    /// Get the option whether to show_parse
    pub fn show_parse(&self) -> bool {
        self.show_parse
    }

    /// Get the option whether to show_definitionss
    pub fn show_definitions(&self) -> bool {
        self.show_definitions
    }
}

impl Default for ReplConfig {
    fn default() -> ReplConfig {
        ReplConfig {
            show_parse: false,
            show_definitions: false,
        }
    }
}

/// Trait to consume AST and produce the output
pub trait StatementConsumer {
    /// process a statmemnt and produce output
    fn consume_statement<W: Write>(
        &mut self,
        writer: &mut W,
        statement: &ReplStatement,
        config: &ReplConfig,
    ) -> Result<(), Box<dyn Error>>;
}

impl<B> Repl<B>
where
    B: StatementConsumer,
{
    /// Create a new REPL
    pub fn new(builder_: B) -> Repl<B> {
        Repl {
            buffer: String::new(),
            builder: builder_,
            cursor: 0,
            config: ReplConfig::default(),
            prompt: DEFAULT_PROMPT,
        }
    }
    /// Handle a command
    pub fn handle_command(&mut self, command: Command) {
        match command {
            Command::ShowParse(b) => {
                self.config.show_parse = b;
            }
            Command::ShowDefinitions(d) => {
                self.config.show_definitions = d;
            }
            Command::Include(f) => {
                let file = match File::open(&f) {
                    Ok(file) => file,
                    Err(err) => {
                        eprintln!("Error opening file {:?}: {:?}", f, err);
                        return;
                    }
                };
                let mut buf_reader = BufReader::new(file);
                match buf_reader.read_to_string(&mut self.buffer) {
                    Ok(bytes) => println!("Read {} bytes from {:?}", bytes, f),
                    Err(err) => eprintln!("Error reading file {:?}: {:?}", f, err),
                }
            }
            Command::PrintBuilderState(_pretty) => {
                unimplemented!("Remember to fix this later!");
                // if pretty {
                //     println!("Builder state: {:#?}", self.builder)
                // } else {
                //     println!("{:?}", self.builder)
                // }
            }
        }
    }
    /// Handle a line of input to the REPL
    pub fn handle_line<W: Write>(
        &mut self,
        line: &str,
        writer: &mut W,
    ) -> Result<bool, Box<dyn Error>> {
        self.buffer.push_str(line);
        self.buffer.push('\n');
        self.prompt = DEFAULT_PROMPT;
        while self.buffer.len() > self.cursor {
            let command = {
                let (rest, statement) = match parse_repl_statement(&self.buffer[self.cursor..]) {
                    Ok(res) => res,
                    Err(err) => match err {
                        Err::Incomplete(_) => {
                            self.prompt = "";
                            return Ok(false);
                        }
                        err => {
                            eprintln!("Parse error: {:?}", err);
                            return Ok(true);
                        }
                    },
                };
                let end = self.buffer.len() - rest.len();
                self.cursor = end;
                match statement {
                    ReplStatement::Command(c) => Some(c),
                    s @ ReplStatement::Statement(_) | s @ ReplStatement::Expr(_) => {
                        self.builder.consume_statement(writer, &s, &self.config)?;
                        None
                    }
                }
            };
            if let Some(command) = command {
                self.handle_command(command)
            }
        }
        Ok(true)
    }
    /// Get a reference to the REPL's buffer
    pub fn buffer(&self) -> &str {
        &self.buffer
    }
    /// Clear the REPL's buffer
    pub fn clear_buffer(&mut self) {
        self.buffer.clear();
        self.cursor = 0;
    }
    /// Get the prompt
    pub fn prompt(&self) -> &'static str {
        self.prompt
    }
}

impl StatementConsumer for Builder<String> {
    fn consume_statement<W: Write>(
        &mut self,
        writer: &mut W,
        statement: &ReplStatement,
        config: &ReplConfig,
    ) -> Result<(), Box<dyn Error>> {
        match statement {
            ReplStatement::Statement(s) => match self.build_statement(&s) {
                Ok(_) => {
                    if config.show_definitions() {
                        writeln!(writer, "Parsed statement: {:?}", s)?;
                    }
                }
                Err(err) => {
                    println!(
                        "Error building let IR: {:#?}\n===========\nAST:\n{:?}\n",
                        err, s
                    );
                }
            },
            ReplStatement::Expr(e) => match self.build_expr(&e) {
                Ok(val) => {
                    if config.show_parse() {
                        writeln!(writer, "Parsed expr: {:?}", e)?;
                    }
                    println!("{}", val)
                }
                Err(err) => println!(
                    "Error building expr IR: {:#?}\n===========\nAST:\n{:?}\n",
                    err, e
                ),
            },
            ReplStatement::Command(_) => panic!("Command should be alreday handled!"),
        }
        Ok(())
    }
}
